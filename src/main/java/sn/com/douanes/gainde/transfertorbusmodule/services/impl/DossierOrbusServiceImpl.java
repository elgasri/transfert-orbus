package sn.com.douanes.gainde.transfertorbusmodule.services.impl;

import com.fasterxml.jackson.databind.ObjectMapper;
import org.springframework.stereotype.Service;
import sn.com.douanes.gainde.transfertorbusmodule.models.DossierOrbus;
import sn.com.douanes.gainde.transfertorbusmodule.services.DossierOrbusService;

import java.io.IOException;

@Service
public class DossierOrbusServiceImpl implements DossierOrbusService {

    public DossierOrbus getJson(String dossier) {

        DossierOrbus dossierJson = new DossierOrbus();

        try {
            ObjectMapper objectMapper = new ObjectMapper();
            dossierJson = objectMapper.readValue(dossier, DossierOrbus.class);
        } catch (IOException err) {
            System.out.printf("Error", err.getMessage());
        }
        return dossierJson;

    }
}
