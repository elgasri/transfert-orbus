package sn.com.douanes.gainde.transfertorbusmodule.services;

import org.springframework.stereotype.Service;
import sn.com.douanes.gainde.transfertorbusmodule.models.DossierOrbus;

@Service
public interface DossierOrbusService {

    DossierOrbus getJson(String dossier);
}
