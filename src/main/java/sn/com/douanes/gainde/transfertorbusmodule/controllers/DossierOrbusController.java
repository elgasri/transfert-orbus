package sn.com.douanes.gainde.transfertorbusmodule.controllers;

import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestPart;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;
import sn.com.douanes.gainde.transfertorbusmodule.models.DossierOrbus;
import sn.com.douanes.gainde.transfertorbusmodule.services.DossierOrbusService;

import java.util.List;

@RestController
public class DossierOrbusController {

    private final DossierOrbusService dossierOrbusService;

    public DossierOrbusController(DossierOrbusService dossierOrbusService) {
        this.dossierOrbusService = dossierOrbusService;
    }

    @PostMapping(value = "/upload", consumes = {
            MediaType.APPLICATION_JSON_VALUE,
            MediaType.MULTIPART_FORM_DATA_VALUE
    })
    public DossierOrbus upload(@RequestPart("dossier") String dossierOrbus, @RequestPart("file") List<MultipartFile> file) {

        DossierOrbus dossierJson = dossierOrbusService.getJson(dossierOrbus);
        return dossierJson;
    }
}
