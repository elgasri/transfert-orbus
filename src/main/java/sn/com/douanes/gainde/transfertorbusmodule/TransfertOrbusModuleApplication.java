package sn.com.douanes.gainde.transfertorbusmodule;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class TransfertOrbusModuleApplication {

    public static void main(String[] args) {
        SpringApplication.run(TransfertOrbusModuleApplication.class, args);
    }

}
