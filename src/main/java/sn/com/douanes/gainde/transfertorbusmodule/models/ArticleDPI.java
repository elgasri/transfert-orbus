package sn.com.douanes.gainde.transfertorbusmodule.models;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import lombok.Getter;
import lombok.Setter;

import java.math.BigDecimal;

@JsonIgnoreProperties(ignoreUnknown = true)
@Getter
@Setter
public class ArticleDPI {

     String numeroDpi;
     Integer numeroArticle;
     String codeProduit;
     String precisionUemoa;
     String paysOrigine;
     String quantite;
     BigDecimal valeurFobArt;
     BigDecimal montantFretArt;
     BigDecimal montantAssuranceArt;
     String unite;
}
