package sn.com.douanes.gainde.transfertorbusmodule.models;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import lombok.Getter;
import lombok.Setter;

@JsonIgnoreProperties(ignoreUnknown = true)
@Getter
@Setter
public class DocumentOrbus {

     String numeroDossierOrbus;
     String codePieceJointe;
     Integer idPole;
     String nomFichier;
     String version;
     String dateFichier;
}
