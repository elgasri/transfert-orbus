package sn.com.douanes.gainde.transfertorbusmodule.models;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import lombok.Getter;
import lombok.Setter;

import java.math.BigDecimal;
import java.util.List;

@JsonIgnoreProperties(ignoreUnknown = true)
@Getter
@Setter
public class DossierOrbus {

     String numeroDossierOrbus;
     String ppmDestinataire;
     String nineaDestinataire;
     String ppmDeclarant;
     String nineaDeclarant;
     String dateEnvoi;
     String numeroDpi;
     String dateDpi;
     String nomvendeur;
     String adressevendeur;
     String villevendeur;
     String telephonevendeur;
     String refproformat;
     String dateproformat;
     String paysProvenance;
     String codeIncoterm;
     BigDecimal valeurFob;
     BigDecimal montantFret;
     BigDecimal montantAssurance;
     String codeBanque;
     BigDecimal montantFacture;
     String deviseFacture;
     Integer nombreArticles;
     List<DocumentOrbus> listeDocuments;
     List<ArticleDPI> articlesDPI;
}
